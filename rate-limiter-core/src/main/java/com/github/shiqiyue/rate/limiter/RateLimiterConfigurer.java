package com.github.shiqiyue.rate.limiter;

import com.github.shiqiyue.rate.limiter.dao.RateLimiterDao;
import com.github.shiqiyue.rate.limiter.intercept.action.RateLimiterInterceptAction;
import com.github.shiqiyue.rate.limiter.redis.name.strategy.RedisKeyNameStrategy;
import org.redisson.api.RedissonClient;

/***
 * 流量控制配置类
 * 
 * @author wwy
 *
 */
public interface RateLimiterConfigurer {
	
	/***
	 * 设置流量控制-拦截后的动作
	 * 
	 * @return
	 */
	public RateLimiterInterceptAction flowControlInterceptAction();
	
	/***
	 * 设置流量控制-拦截数据来源
	 * 
	 * @return
	 */
	public RateLimiterDao flowControlDao();
	
	/***
	 * 设置redis命名策略
	 * 
	 * @return
	 */
	public RedisKeyNameStrategy redisKeyNameStrategy();
	
	/***
	 * 设置redis客户端
	 * 
	 * @return
	 */
	public RedissonClient redissonClient();
	
}
