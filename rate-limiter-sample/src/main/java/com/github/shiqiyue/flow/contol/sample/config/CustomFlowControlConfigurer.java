package com.github.shiqiyue.flow.contol.sample.config;

import com.github.shiqiyue.rate.limiter.RateLimiterConfigurer;
import com.github.shiqiyue.rate.limiter.dao.InMemoryRateLimiterDao;
import com.github.shiqiyue.rate.limiter.dao.RateLimiterDao;
import com.github.shiqiyue.rate.limiter.entity.RateLimiterItem;
import com.github.shiqiyue.rate.limiter.intercept.action.DefaultRateLimiterInterceptAction;
import com.github.shiqiyue.rate.limiter.intercept.action.RateLimiterInterceptAction;
import com.github.shiqiyue.rate.limiter.redis.name.strategy.DefaultRedisKeyNameStrategy;
import com.github.shiqiyue.rate.limiter.redis.name.strategy.RedisKeyNameStrategy;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/***
 * 自定义流量控制配置信息
 * 
 * @author wwy
 *
 */
@Component
public class CustomFlowControlConfigurer implements RateLimiterConfigurer {
	
	@Autowired
	private RedissonClient redissonClient;
	
	@Override
	public RedissonClient redissonClient() {
		return redissonClient;
	}
	
	@Override
	public RateLimiterInterceptAction flowControlInterceptAction() {
		return new DefaultRateLimiterInterceptAction();
	}
	
	@Override
	public RateLimiterDao flowControlDao() {
		InMemoryRateLimiterDao flowControlDao = new InMemoryRateLimiterDao();
		flowControlDao.insert(new RateLimiterItem("/test/f1/**", 3, 1000L));
		return new InMemoryRateLimiterDao();
	}
	
	@Override
	public RedisKeyNameStrategy redisKeyNameStrategy() {
		return new DefaultRedisKeyNameStrategy();
	}
	
}
